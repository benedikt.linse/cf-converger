#!/bin/bash

set -u
set -x

curl -H "Content-Type: application/x-yaml" \
     -k \
     -X POST \
     --data-binary @src/test/resources/fixtures/declaration.yml \
     $CF_CONVERGER_ENDPOINT
