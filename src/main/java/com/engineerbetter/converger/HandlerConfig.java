package com.engineerbetter.converger;

import com.engineerbetter.converger.facade.*;
import com.engineerbetter.converger.intents.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.core.type.TypeReference;

@Configuration
public class HandlerConfig
{
	@Bean
	public HandlerFactory handlerFactory(CloudFoundryFacade cf, UaaFacade uaa)
	{
		TypeReferenceMapHandlerFactory handlerFactory = new TypeReferenceMapHandlerFactory();
		handlerFactory.put(new TypeReference<HandlerBuilder<OrgIntent>>() {}, new CfFacadeOrgHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<UserOrgIntent>>() {}, new CfFacadeUserOrgHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<CfUserIntent>>() {}, new CfFacadeCfUserHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<UaaUserIntent>>() {}, new UaaFacadeUaaUserHandlerBuilder(uaa));
		handlerFactory.put(new TypeReference<HandlerBuilder<OrgManagerIntent>>() {}, new CfFacadeOrgManagerHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<OrgAuditorIntent>>() {}, new CfFacadeOrgAuditorHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<SpaceIntent>>() {}, new CfFacadeSpaceHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<SpaceAuditorIntent>>() {}, new CfFacadeSpaceAuditorHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<SpaceDeveloperIntent>>() {}, new CfFacadeSpaceDeveloperHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<SpaceManagerIntent>>() {}, new CfFacadeSpaceManagerHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<UpsIntent>>() {}, new CfFacadeUpsHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<SecurityGroupIntent>>() {}, new CfFacadeSecurityGroupHandlerBuilder(cf));
		handlerFactory.put(new TypeReference<HandlerBuilder<SpaceSecurityGroupIntent>>() {}, new CfFacadeSpaceSecurityGroupHandlerBuilder(cf));
		return handlerFactory;
	}
}
