package com.engineerbetter.converger.facade;

import com.engineerbetter.converger.intents.Handler;
import com.engineerbetter.converger.intents.SpaceSecurityGroupIntent;

public class CfFacadeSpaceSecurityGroupHandlerBuilder extends CfFacadeHandlerBuilder<SpaceSecurityGroupIntent>
{

	public CfFacadeSpaceSecurityGroupHandlerBuilder(CloudFoundryFacade cf)
	{
		super(cf);
	}

	@Override
	public Handler<SpaceSecurityGroupIntent> build(SpaceSecurityGroupIntent intent)
	{
		return new CfFacadeSpaceSecurityGroupHandler(intent, cf);
	}
}
