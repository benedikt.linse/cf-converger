package com.engineerbetter.converger.facade;

import com.engineerbetter.converger.intents.SecurityGroupHandler;
import com.engineerbetter.converger.intents.SecurityGroupIntent;
import com.engineerbetter.converger.model.SecurityGroupRule;
import com.engineerbetter.converger.resolution.MutableResolution;
import org.cloudfoundry.client.v2.securitygroups.Protocol;
import org.cloudfoundry.client.v2.securitygroups.RuleEntity;
import org.cloudfoundry.client.v2.securitygroups.SecurityGroupEntity;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CfFacadeSecurityGroupHandler extends SecurityGroupHandler
{
	private final CloudFoundryFacade cf;


	public CfFacadeSecurityGroupHandler(SecurityGroupIntent intent, CloudFoundryFacade cf)
	{
		super(intent);
		this.cf = cf;
	}

	@Override
	public void resolve() {
		Optional<String> securityGroupId = cf.findSecurityGroup(intent.name.name);
		if (securityGroupId.isPresent()) {
			SecurityGroupEntity observed = cf.getSecurityGroup(securityGroupId.get());
			if (equivalent(observed.getRules(), intent.securityGroupRules)) {
				intent.setResolution(MutableResolution.same(securityGroupId.get()));
			} else {
				Javers javers = JaversBuilder.javers().build();
				Diff diff = javers.compare(observed, intent.securityGroupRules);
				intent.setResolution(MutableResolution.different(securityGroupId.get(), diff.getChanges()));
			}
		} else {
			intent.setResolution(MutableResolution.absent());
		}
	}

	private boolean equivalent(List<RuleEntity> ruleEntities, List<SecurityGroupRule> securityGroupRules) {
        List<SecurityGroupRule> mapped = ruleEntities.stream().map(this::map).collect(Collectors.toList());
        return (
                mapped.size() == securityGroupRules.size() &&
                mapped.containsAll(securityGroupRules) &&
                securityGroupRules.containsAll(mapped)
        );
    }

    private SecurityGroupRule map(RuleEntity ruleEntity) {
	    return new SecurityGroupRule(
	            ruleEntity.getDestination(),
                ruleEntity.getProtocol().getValue(),
                ruleEntity.getDescription()
        );
    }

	@Override
	public void converge() {
		if(! intent.getResolution().exists()) {
			String securityGroupId = cf.createSecurityGroup(intent.name.name, intent.securityGroupRules);
			intent.setResolution(MutableResolution.same(securityGroupId));
		}
        else if(intent.getResolution().hasDifferences()) {
            cf.updateSecurityGroup(intent.getResolution().getId().get(), intent.name.name,
                    intent.securityGroupRules.stream().map(
                            rule -> RuleEntity.builder()
                                    .destination(rule.destination)
                                    .protocol(Protocol.from(rule.protocol))
                                    // .ports(rule.ports) // the java client currently does not correctly deal with ports.
                                    .description(rule.description)
                                    .build()
                    ).collect(Collectors.toList())
            );
        }
	}

}
