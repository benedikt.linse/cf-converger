package com.engineerbetter.converger.facade;

import java.util.List;
import java.util.Optional;

import com.engineerbetter.converger.model.SecurityGroupRule;
import com.engineerbetter.converger.properties.UpsProperties;
import org.cloudfoundry.client.v2.securitygroups.RuleEntity;
import org.cloudfoundry.client.v2.securitygroups.SecurityGroupEntity;

public interface CloudFoundryFacade
{

	SecurityGroupEntity getSecurityGroup(String s);

    public enum OrgRole
	{
		MANAGER,
		AUDITOR
	}

	public enum SpaceRole
	{
		MANAGER,
		DEVELOPER,
		AUDITOR
	}

	Optional<String> findSecurityGroup(String name);
	String createSecurityGroup(String name, List<SecurityGroupRule> securityGroupRules);
	void bindSpaceSecurityGroup(String securityGroupId, String spaceId);
	boolean isSecurityGroupBoundToSpace(String securityGroupId, String spaceId);
	void updateSecurityGroup(String securityGroupId, String securityRuleName, List<RuleEntity> ruleEntities);

	Optional<String> findOrg(String name);
	String createOrg(String name);
	void deleteOrg(String id);


	Optional<String> findSpace(String name, String orgId);
	String createSpace(String name, String orgId);
	void deleteSpace(String id);

	Optional<String> findUps(String name, String spaceId);
	String createUps(UpsProperties properties, String spaceId);
	UpsProperties getUps(String id);
	void updateUps(UpsProperties properties, String spaceId);
	void deleteUps(String id);

	boolean userExists(String id);
	void createUser(String id);
	void deleteUser(String id);

	boolean isUserInOrg(String userId, String orgId);
	void addUserToOrg(String userId, String orgId);
	boolean hasOrgRole(String userId, String orgId, OrgRole role);
	void setOrgRole(String userId, String orgId, OrgRole role);

	boolean hasSpaceRole(String userId, String spaceId, SpaceRole role);
	void setSpaceRole(String userId, String spaceId, SpaceRole role);
}
