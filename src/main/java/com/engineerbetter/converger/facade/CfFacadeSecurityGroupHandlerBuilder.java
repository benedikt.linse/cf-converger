package com.engineerbetter.converger.facade;

import com.engineerbetter.converger.intents.Handler;
import com.engineerbetter.converger.intents.SecurityGroupIntent;
import com.engineerbetter.converger.intents.UpsIntent;

public class CfFacadeSecurityGroupHandlerBuilder extends CfFacadeHandlerBuilder<SecurityGroupIntent>
{

	public CfFacadeSecurityGroupHandlerBuilder(CloudFoundryFacade cf)
	{
		super(cf);
	}

	@Override
	public Handler<SecurityGroupIntent> build(SecurityGroupIntent intent)
	{
		return new CfFacadeSecurityGroupHandler(intent, cf);
	}
}
