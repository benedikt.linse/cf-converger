package com.engineerbetter.converger.facade;

import com.engineerbetter.converger.intents.SpaceSecurityGroupHandler;
import com.engineerbetter.converger.intents.SpaceSecurityGroupIntent;
import com.engineerbetter.converger.resolution.RelationshipResolution;

import java.util.Optional;

public class CfFacadeSpaceSecurityGroupHandler extends SpaceSecurityGroupHandler
{
	private final CloudFoundryFacade cf;

	CfFacadeSpaceSecurityGroupHandler(SpaceSecurityGroupIntent intent, CloudFoundryFacade cf) {
		super(intent);
		this.cf = cf;
	}

	@Override
	public void resolve() {
		Optional<String> spaceId = intent.spaceIntent.getResolution().getId();
		Optional<String> securityGroupId = intent.securityGroupIntent.getResolution().getId();

		if(spaceId.isPresent() && securityGroupId.isPresent()) {
			intent.setResolution(
					RelationshipResolution.of(
							cf.isSecurityGroupBoundToSpace(securityGroupId.get(), spaceId.get())
					)
			);
		}
		else {
			intent.setResolution(RelationshipResolution.absent());
		}
	}

	@Override
	public void converge() {
		if(! intent.getResolution().exists()) {
			cf.bindSpaceSecurityGroup(
					intent.securityGroupIntent.getResolution().getId().get(),
					intent.spaceIntent.getResolution().getId().get()
			);
		}
	}

}
