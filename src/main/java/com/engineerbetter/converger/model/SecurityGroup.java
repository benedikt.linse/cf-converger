package com.engineerbetter.converger.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SecurityGroup {

	public final String name;
	public final List<SecurityGroupRule> rules;

	@JsonCreator
	public SecurityGroup(
        @JsonProperty("name") String name,
        @JsonProperty("rules") List<SecurityGroupRule> rules
	) {
	    this.name = name;
	    this.rules = rules;
    }

}
