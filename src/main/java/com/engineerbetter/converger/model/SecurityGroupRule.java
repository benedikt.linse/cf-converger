package com.engineerbetter.converger.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class SecurityGroupRule {

    public final String destination;
    public final String protocol;
    public final String description;

    @JsonCreator
    public SecurityGroupRule(
            @JsonProperty("destination") String destination,
            @JsonProperty("protocol") String protocol,
            @JsonProperty("description") String description
    ) {
        this.destination = destination;
        this.protocol = protocol;
        this.description = description;
    }

}
