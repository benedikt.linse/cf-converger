package com.engineerbetter.converger.intents;

import com.engineerbetter.converger.model.SecurityGroupRule;
import com.engineerbetter.converger.properties.NameProperty;
import com.engineerbetter.converger.resolution.IdentifiableResolution;
import com.engineerbetter.converger.resolution.MutableResolution;

import java.util.List;

public class SecurityGroupIntent implements Intent<MutableResolution>
{
	public final NameProperty name;
	private MutableResolution resolution;
	public final List<SecurityGroupRule> securityGroupRules;

	public SecurityGroupIntent(NameProperty name, List<SecurityGroupRule> securityGroupRules)
	{
		this.name = name;
		this.securityGroupRules = securityGroupRules;
	}

	@Override
	public MutableResolution getResolution()
	{
		return resolution;
	}

	@Override
	public void setResolution(MutableResolution resolution)
	{
		this.resolution = resolution;
	}


	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecurityGroupIntent other = (SecurityGroupIntent) obj;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "SecurityGroupIntent [name=" + name + "]";
	}
}
