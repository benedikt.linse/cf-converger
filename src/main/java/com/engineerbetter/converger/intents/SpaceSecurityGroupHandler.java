package com.engineerbetter.converger.intents;

public abstract class SpaceSecurityGroupHandler extends Handler<SpaceSecurityGroupIntent>
{
	public SpaceSecurityGroupHandler(SpaceSecurityGroupIntent intent)
	{
		super(intent);
	}


	@Override
	public String getPlanAction()
	{
		String not = intent.getResolution().exists() ? "not " : "";
		return "Would "+not+"bind "+intent.securityGroupIntent.name.name+" to space "+intent.spaceIntent.name.name;
	}
}
