package com.engineerbetter.converger.intents;

public abstract class SecurityGroupHandler extends Handler<SecurityGroupIntent>
{
	public SecurityGroupHandler(SecurityGroupIntent intent)
	{
		super(intent);
	}
}
