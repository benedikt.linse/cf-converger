package com.engineerbetter.converger.intents;

import com.engineerbetter.converger.resolution.RelationshipResolution;


public class SpaceSecurityGroupIntent implements Intent<RelationshipResolution>
{
	public final SpaceIntent spaceIntent;
	public final SecurityGroupIntent securityGroupIntent;
	private RelationshipResolution resolution;

	public SpaceSecurityGroupIntent(SpaceIntent space, SecurityGroupIntent securityGroupIntent)
	{
		this.spaceIntent = space;
		this.securityGroupIntent = securityGroupIntent;
	}

	@Override
	public RelationshipResolution getResolution()
	{
		return resolution;
	}

	@Override
	public void setResolution(RelationshipResolution resolution)
	{
		this.resolution = resolution;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((spaceIntent == null) ? 0 : spaceIntent.hashCode());
		result = prime * result
				+ ((securityGroupIntent == null) ? 0 : securityGroupIntent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpaceSecurityGroupIntent other = (SpaceSecurityGroupIntent) obj;
		if (spaceIntent == null)
		{
			if (other.spaceIntent != null)
				return false;
		} else if (!spaceIntent.equals(other.spaceIntent))
			return false;
		if (securityGroupIntent == null)
		{
			if (other.securityGroupIntent != null)
				return false;
		} else if (!securityGroupIntent.equals(other.securityGroupIntent))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "SpaceSecurityGroupIntent [spaceIntent=" + spaceIntent
				+ ", securityGroupIntent=" + securityGroupIntent + "]";
	}
}
