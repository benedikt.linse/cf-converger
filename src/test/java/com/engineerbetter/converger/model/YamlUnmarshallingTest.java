package com.engineerbetter.converger.model;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;

public class YamlUnmarshallingTest
{
	@Test
	public void unmarshallsCorrectly() throws Exception
	{
		ClassPathResource resource = new ClassPathResource("fixtures/declaration.yml");
		YAMLMapper mapper = new YAMLMapper();
		Declaration declaration = mapper.readValue(resource.getFile(), Declaration.class);

		assertThat(declaration.schemaVersion, is(2));
		assertThat(declaration.org.name, is("my-lovely-org"));
		assertNotNull(declaration.securityGroups);
		assertEquals(2, declaration.securityGroups.size());
		assertEquals("mysql_security_group", declaration.securityGroups.get(0).name);
		assertEquals("all", declaration.securityGroups.get(0).rules.get(0).protocol);
		assertEquals("all", declaration.securityGroups.get(0).rules.get(1).protocol);
		assertNotNull(declaration.securityGroups.get(0).rules.get(0).destination);
		assertEquals(2, declaration.securityGroups.get(0).rules.size());
	}
}
