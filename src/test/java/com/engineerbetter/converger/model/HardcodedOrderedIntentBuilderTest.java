package com.engineerbetter.converger.model;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.engineerbetter.converger.intents.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import com.engineerbetter.converger.HandlerConfig;
import com.engineerbetter.converger.facade.CloudFoundryFacade;
import com.engineerbetter.converger.facade.UaaFacade;
import com.engineerbetter.converger.properties.NameProperty;
import com.engineerbetter.converger.properties.UaaUserProperties;
import com.engineerbetter.converger.resolution.Resolution;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;

public class HardcodedOrderedIntentBuilderTest {

    private YAMLMapper mapper = new YAMLMapper();
    private ClassPathResource resource = new ClassPathResource("fixtures/declaration.yml");
    private Declaration fixture = mapper.readValue(resource.getFile(), Declaration.class);
    private CloudFoundryFacade cf = mock(CloudFoundryFacade.class);
    private UaaFacade uaa = mock(UaaFacade.class);
    private HandlerFactory handlerFactory = new HandlerConfig().handlerFactory(cf, uaa);
	private OrderedHandlerBuilder builder = new HardcodedOrderedHandlerBuilder(handlerFactory);
	private OrgIntent org = new OrgIntent(new NameProperty("my-lovely-org"));
    private SpaceIntent devSpace = new SpaceIntent(new NameProperty("DEV"), org);
    private SpaceIntent prodSpace = new SpaceIntent(new NameProperty("PROD"), org);
    private Handler<SpaceIntent> devSpaceHandler = handlerFactory.build(devSpace);
    private Handler<SpaceIntent> prodSpaceHandler = handlerFactory.build(prodSpace);
    private Handler<OrgIntent> orgHandler = handlerFactory.build(org);
    private UaaUserIntent uaaDanYoung = new UaaUserIntent(new UaaUserProperties("dan.young@example.com", "Dan", "Young"));
    private CfUserIntent cfDanYoung = new CfUserIntent(uaaDanYoung);
    private UaaUserIntent uaaDanJones = new UaaUserIntent(new UaaUserProperties("daniel.jones@example.com", "Daniel", "Jones"));
    private CfUserIntent cfDanJones = new CfUserIntent(uaaDanJones);
    private Handler<CfUserIntent> cfDanYoungHandler = handlerFactory.build(cfDanYoung);
    private Handler<CfUserIntent> cfDanJonesHandler = handlerFactory.build(cfDanJones);
    private UserOrgIntent danYoungOrg = new UserOrgIntent(org, cfDanYoung);
    private UserOrgIntent danJonesOrg = new UserOrgIntent(org, cfDanJones);
    private Handler<UserOrgIntent> danYoungOrgHandler = handlerFactory.build(danYoungOrg);
    private Handler<UserOrgIntent> danJonesOrgHandler = handlerFactory.build(danJonesOrg);
    private List<Handler<? extends Intent<? extends Resolution>>> handlers = builder.getOrderedHandlers(fixture);
    private SecurityGroupIntent defaultSecurityGroup = new SecurityGroupIntent(
            new NameProperty("default_security_group"),
            Arrays.asList(
                    new SecurityGroupRule("0.0.0.0-169.253.255.255", "all", "description"),
                    new SecurityGroupRule("169.255.0.0-255.255.255.255", "all", "description")
            )
    );
    private Handler<SecurityGroupIntent> defaultSecurityGroupHandler = handlerFactory.build(defaultSecurityGroup);
    private SpaceSecurityGroupIntent spaceSecurityGroupIntent = new SpaceSecurityGroupIntent(devSpace, defaultSecurityGroup);
    private Handler<SpaceSecurityGroupIntent> devSpaceDefaultSecGroupHandler = handlerFactory.build(spaceSecurityGroupIntent);

    public HardcodedOrderedIntentBuilderTest() throws IOException {
    }

    @Test
    public void aHandlerForTheDefaultSecurityGroupShouldBeGenerated() {
        assertThat(handlers, hasItem(defaultSecurityGroupHandler));
    }

    @Test
    public void aHandlerForTheOrganizationShouldBeGenerated() {
        assertThat(handlers, hasItem(orgHandler));
    }

    @Test
    public void handlersForBothSpacesShouldBeGenerated() {
        assertThat(handlers, hasItem(devSpaceHandler));
        assertThat(handlers, hasItem(prodSpaceHandler));
    }

    @Test
    public void orgsShouldBeCreatedPriorToTheirSpaces() {
        assertBefore(orgHandler, devSpaceHandler, handlers);
        assertBefore(orgHandler, prodSpaceHandler, handlers);
    }

    @Test
    public void bindingOfSecurityGroupsToSpacesShouldHappenOnlyAfterCreationOfTheAssociatedSpaceAndSecurityGroup() {
        assertBefore(defaultSecurityGroupHandler, devSpaceDefaultSecGroupHandler, handlers);
        assertBefore(devSpaceHandler, devSpaceDefaultSecGroupHandler, handlers);
    }

    @Test
    public void usersShouldBeCreatedFirstViaTheUaaApiThenViaTheCCApi() {
        Handler<UaaUserIntent> uaaDanYoungHandler = handlerFactory.build(uaaDanYoung);
        Handler<UaaUserIntent> uaaDanJonesHandler = handlerFactory.build(uaaDanJones);
        assertBefore(uaaDanYoungHandler, cfDanYoungHandler, handlers);
        assertBefore(uaaDanJonesHandler, cfDanJonesHandler, handlers);
    }

	@Test
	public void usersShouldBeAssignedToOrgsOnlyAfterTheCorrespondingUsersAndOrgsAreCreated() {
        assertBefore(orgHandler, danYoungOrgHandler, handlers);
        assertBefore(cfDanYoungHandler, danYoungOrgHandler, handlers);
        assertBefore(orgHandler, danJonesOrgHandler, handlers);
        assertBefore(cfDanJonesHandler, danJonesOrgHandler, handlers);
    }

    @Test
    public void orgRolesShouldBeAssignedOnlyAfterTheAssociatedOrgsAndUsersAreCreated() {
        OrgManagerIntent orgManager = new OrgManagerIntent(org, cfDanYoung);
        Handler<OrgManagerIntent> orgManagerHandler = handlerFactory.build(orgManager);
        assertBefore(orgHandler, orgManagerHandler, handlers);
        assertBefore(cfDanYoungHandler, orgManagerHandler, handlers);
    }

    @Test
    public void spaceRolesShouldBeAssignedOnlyAfterTheAssociatedSpacesAndUsersAreCreated() {
		SpaceDeveloperIntent devSpaceDeveloper = new SpaceDeveloperIntent(devSpace, cfDanJones);
		Handler<SpaceDeveloperIntent> devSpaceDeveloperHandler = handlerFactory.build(devSpaceDeveloper);
		assertBefore(devSpaceHandler, devSpaceDeveloperHandler, handlers);
		assertBefore(danJonesOrgHandler, devSpaceDeveloperHandler, handlers);

		SpaceManagerIntent devSpaceManager = new SpaceManagerIntent(devSpace, cfDanJones);
		Handler<SpaceManagerIntent> devSpaceManagerHandler = handlerFactory.build(devSpaceManager);
		assertBefore(devSpaceHandler, devSpaceManagerHandler, handlers);
		assertBefore(danJonesOrgHandler, devSpaceManagerHandler, handlers);

		SpaceDeveloperIntent prodSpaceDeveloper = new SpaceDeveloperIntent(prodSpace, cfDanJones);
		Handler<SpaceDeveloperIntent> prodSpaceDeveloperHandler = handlerFactory.build(prodSpaceDeveloper);
		assertBefore(prodSpaceHandler, prodSpaceDeveloperHandler, handlers);
		assertBefore(danJonesOrgHandler, prodSpaceDeveloperHandler, handlers);
	}


	private void assertBefore(
	        Handler<? extends Intent<? extends Resolution>> before,
            Handler<? extends Intent<? extends Resolution>> after,
            List<Handler<? extends Intent<? extends Resolution>>> handlers) {
		assertThat(handlers, hasItem(before));
		assertThat(handlers, hasItem(after));

		int beforeIndex = -1;
		int afterIndex = -1;

		for(int index = 0; index < handlers.size(); index++) {
			Handler<? extends Intent<? extends Resolution>> current = handlers.get(index);
			if(current.equals(before)) {
				beforeIndex = index;
			}

			if(current.equals(after)) {
				afterIndex = index;
			}
		}

		assertThat("Handler did not appear in list", beforeIndex, not(-1));
		assertThat("Handler did not appear in list", afterIndex, not(-1));
		assertThat("Handlers appeared to be the same", beforeIndex, not(afterIndex));
		assertThat(before+" did not appear before "+after, beforeIndex, lessThan(afterIndex));
	}
}
